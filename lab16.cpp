﻿// lab16.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cassert> 


class Matrix
{
private:
    double m_data[5][5]{};

public:
    double& operator()(int row, int col);
    double operator()(int row, int col) const;
    void operator()();
};

double& Matrix::operator()(int row, int col)
{
    assert(col >= 0 && col < 5);
    assert(row >= 0 && row < 5);

    return m_data[row][col];
}

double Matrix::operator()(int row, int col) const
{
    assert(col >= 0 && col < 5);
    assert(row >= 0 && row < 5);

    return m_data[row][col];
}

void Matrix::operator()()
{
    for (int row{ 0 }; row < 5; ++row)
    {
        for (int col{ 0 }; col < 5; ++col)
        {
            m_data[row][col] = 0.0;
        }
    }
}

int main()
{
    Matrix matrix{};

    matrix(1, 2) = 4.5;
    matrix();
    std::cout << matrix(1, 2) << '\n';

    return 0;
}
